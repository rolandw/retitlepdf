#!/bin/bash

# Written by roland <roland@chart-shed.com>
# Last modified: 1st December 2020

showhelp() {
    echo "retitlepdf - change the title of the pdf to the file name less file extension"
    echo "usage: $retitlepdf [-h][-t "Title"] <file>|<folder>"
    echo ""
    echo "       This script has been tested against Ubuntu 20. You might need to make changes if"
    echo "       you are running on a different distribution."
    echo ""
    echo "       try flag -v for debug/verbose mode."
    echo ""
    echo "example: retitlepdf downloads/mydocument.pdf"

    if [[ $verbose ]]; then
	echo ""
	echo "You are running in debug mode"
    fi

    exit 1
}

retitle() {
    if [ $# -eq 0 ]; then
        result="Error: no file path provided"
    else
        if [[ $customTitle ]]; do
    
    
    res=$(find /usr/share/applications ~/.local/share/applications -iname "*$1*" -exec grep -i exec {} \; | head -1 | cut -d "=" -f 2 | cut -d " " -f 1)
       if [ $? -ne 0 ]; then
          result="Error finding application: $res"
       elif [ -z $res ] ; then
           result="Error: application $1 not found"
       else
           result=$res
       fi
    fi
    echo $result
}

getTitle() {
    if [ $# -eq 0 ]; then
        result="Error: no file"
}


while getopts ":a:vh?" opts; do
    case "${opts}" in
	    h|\?)
	        showhelp
	        ;;
	    v)
	        verbose=True
	        ;;
	    t)
	        customTitle=True
            title=${OPTARG}
            ;;
    esac
done
 
shift $(($OPTIND -1))
while [[ $1 && ${1} ]]; do
    path=$(printf %q "${1}")
    if [[ -d $path ]]; then
    echo "$path is a directory"
elif [[ -f $path ]]; then
    echo "$path is a file"
else
    echo "$path is not valid"
    exit 1
fi
    if [ $customTitle ]; then
        if [[ $executable == Error* ]] ; then
            echo $executable
        elif [ -z $executable ] ; then
            if [[ $verbose ]]; then
                echo "No such application was found"
            fi
        else
            if [[ $verbose ]] ; then
                errorHandler="2>/dev/null"
                echo "Running command: $executable $filepath $errorHandler"
            else
                errorHandler=""
            fi
            sh -c "$executable $filepath $errorHandler &"
        fi
    else
        if [[ $verbose ]]; then
            echo "About to open file $filepath with the default app"
        fi
        xdg-open $filepath & 
    fi
    last=${1}
    shift 1
    if [ "$1" == "$last" ]; then
	break
    fi
done
