# retitlepdf

## Background

Retitlepdf is a bash script to change the title of a pdf to the file name without the file extension. If you pass the -t flag and string then it will use that string to set the title. If the script is run on a directory then it will rename all pdfs within that folder.

Retitlepdf was first written when I noticed that "Document Viewer", the default PDF viewer in Ubuntu 20, uses the title of the pdf in the list of open files and I had open a number of system generated PDFs which had rubbish titles.

Please note that this script makes use of libimage-exiftool-perl.

## Installation

At the moment there is no deb/rpm or similar. Clone the project (```git clone git@gitlab.com:rolandw/retitlepdf.git```). cd to the project directory and run ```chmod +x retitlepdf```. Put a symlink to the file into a directory in your path. From experience, I generally use /usr/local/bin. If you want the man page installed, cd to your man path (usually /usr/local/share/man/man1) and add a symlink to the retitlepdf.1.gz file in the project directory. Then run ```makewhatis``` to update the man file cache.

If you do not have libimage-exiftool-perl installed then please run ```sudo apt install libimage-exiftool-perl``` if running on an apt based system (Debian, Ubuntu) or ```sudo yum install libimage-exiftool-perl``` if you are a Red Hat user.

## Usage

```retitlepdf [-h][-t title] <file>|<directory>```

-t uses the supplied title string to retitle the document. Note that when selecting multiple files or a directory, all will have the same name.

-h will print the help messages.

### Examples

``` retitlepdf mypdf.pdf```

will change the title of mypdf.pdf to "mypdf"


``` retitlepdf -t "The best ever document" mypdf.pdf ```

will change the title of mypdf.pdf to "The best ever document"


``` retitlepdf  myproject/ ```

will change the titles of all documents within the myproject directory to the filename without the file extension.



## Testing

This script has been tested against Ubuntu 20. You might need to make changes if you are running on a different distribution.
